POST /token/posts/{post_id}/comments
comment="The comment message"


GET /posts/{post_id}/comments

@returns JSON

[
	{
		"comment" : "comment content",
		"user_name" : "user_name",
		"id" : comment_id,
		"user_picture" : "picture url"
	}
]


POST /{token}/posts/{post_id}/like

@returns STATUS 200 liked
@returns STATUS 201 Already liked


POST /{token}/post

@type multipart request
image=image entity
description=description

@returns STATUS 200 ok


POST /register
phone=phone number (+98|0|null|0098)9*********

@returns STATUS 200 ok

POST /verify
phone=phone number
code=message code

@returns JSON

{"token" : "user token", "type" : "admin|user"}

@desc only admin user has permission to create a new post