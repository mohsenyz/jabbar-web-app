package dottech.web;

import dottech.web.controllers.*;
import dottech.web.utils.AsyncTask;
import dottech.web.utils.HibernateUtils;

import java.io.BufferedReader;
import java.io.InputStream;

import static spark.Spark.*;

public class Main {

    public static void main(String[] args) {

        initDB();

        staticFiles.location("/public");
        staticFiles.expireTime(60 * 5);

        port(8080);

        get("/posts", MainController::getPosts);
        get("/posts/:post_id/comments", CommentController::getCommentsByPost);
        post("/:token/posts/:post_id/like", LikeController::likePost);
        get("/:token/posts/:post_id/like", LikeController::likeStatusByPost);
        post("/:token/posts/:post_id/comment", CommentController::newComment);
        post("/:token/post", PostController::newPost);
        get("/images/:image", ImageController::serveImage);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            AsyncTask.shutdownNow();
        }));
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            AsyncTask.shutdownNow();
        });
    }

    public static void initDB() {
        HibernateUtils.getSessionFactory().getCurrentSession();
    }

}
