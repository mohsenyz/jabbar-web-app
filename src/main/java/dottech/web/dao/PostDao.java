package dottech.web.dao;

import dottech.web.pojos.Post;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class PostDao extends BaseDao<Post> {

    public PostDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public void save(Post post) {
        if (post.id != null) {
            super.update(post);
        } else {
            super.save(post);
        }
    }


    public List<Post> findAll() {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Post");
        List<Post> list = new ArrayList<>();
        try {
            list = query.getResultList();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        }
        return list;
    }
}
