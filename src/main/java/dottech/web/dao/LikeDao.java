package dottech.web.dao;

import dottech.web.pojos.Like;
import dottech.web.pojos.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;

public class LikeDao extends BaseDao<Like> {

    public LikeDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }


    public boolean toggleLikeByPost(long postId, User user) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM `Like` L WHERE L.post_id = :post_id AND L.user_id = :user_id");
        query.setParameter("post_id", postId);
        query.setParameter("user_id", user.id);
        Like like = null;
        try {
            like = (Like) query.getSingleResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        }

        if (like == null) {
            like = new Like();
            like.created_at = System.currentTimeMillis() / 1000l;
            like.post_id = postId;
            like.user_id = user.id;

            save(like);
            return true;
        } else {
            delete(like);
            return false;
        }
    }


    public Like findByPost(long postId, long userId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM `Like` L WHERE L.post_id = :post_id AND L.user_id = :user_id");
        query.setParameter("post_id", postId);
        query.setParameter("user_id", userId);
        Like like = null;
        try {
            like = (Like) query.getSingleResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        }
        return like;
    }
}
