package dottech.web.dao;

import dottech.web.pojos.Comment;
import dottech.web.pojos.Post;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class CommentDao extends BaseDao<Comment> {

    public CommentDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }


    public List<Comment> findByPost(long postId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Comment C WHERE C.post_id = :id");
        query.setParameter("id", postId);
        List<Comment> list = new ArrayList<>();
        try {
            list = query.getResultList();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        }
        return list;
    }
}
