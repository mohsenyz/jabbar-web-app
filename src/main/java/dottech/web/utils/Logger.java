package dottech.web.utils;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Logger {
    private static ArrayBlockingQueue queue = new ArrayBlockingQueue(Integer.MAX_VALUE);

    public static void log(String key, Throwable t) {
        queue.add(LogNode.from(key, t));
    }


    static {
        new Thread(() -> {
            try {
                LogNode logNode;
                while ((logNode = (LogNode) queue.take()) != null) {
                    // Write the log node in where ever you want
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }


    public static class LogNode {

        public LogNode(String key, Throwable t) {
            this.key = key;
            this.t = t;
        }

        String key;
        Throwable t;

        public static LogNode from(String key, Throwable t) {
            return new LogNode(key, t);
        }
    }
}
