package dottech.web.utils;

public class AppConfig {

    public static final String CONFIG_FILE = System.getenv("CONFIG_FILE");
    public static final String DEFAULT_CONFIG_FILE = "config.properties";
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin*@123";

    public static final int maxImageSize() {
        return Integer.parseInt(AppProperties.getProp().getProperty(AppProperties.KEY_MAX_IMAGE_SIZE_MB)) * 1048576;
    }

}

