package dottech.web.utils;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class AsyncTask {

    public static final int THREADS_NUM = 16;

    private static ExecutorService executor = Executors.newFixedThreadPool(16);

    public static void run(Runnable runnable) {
        executor.execute(runnable);
    }

    public static void shutdownNow() {
        executor.shutdownNow();
    }
}