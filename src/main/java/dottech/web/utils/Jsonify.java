package dottech.web.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Response;

import java.io.IOException;

public class Jsonify {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String toJson(Object value) throws IOException {
        return objectMapper.writeValueAsString(value);
    }

    public static String returnJson(Response response, Object value) throws Exception {
        response.type("application/json");
        return toJson(value);
    }

}
