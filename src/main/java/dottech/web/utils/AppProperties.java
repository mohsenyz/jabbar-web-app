package dottech.web.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AppProperties {

    private static Properties prop;

    public static final String KEY_REDIS_ENABLED = "redis.enabled";
    public static final String KEY_REDIS_HOST = "redis.host";
    public static final String KEY_MAX_IMAGE_SIZE_MB = "app.max_image_size_mb";
    public static final String KEY_UPLLOAD_DIR = "app.upload_dir";

    static {
        prop = new Properties();
        try {
            if (AppConfig.CONFIG_FILE != null) {
                prop.load(new FileInputStream(AppConfig.CONFIG_FILE));
            } else {
                prop.load(AppProperties.class.getClassLoader().getResourceAsStream(AppConfig.DEFAULT_CONFIG_FILE));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static Properties getProp() {
        for (String property : prop.stringPropertyNames()) {
            String envProperty = System.getenv(property);
            if (envProperty != null && !envProperty.trim().isEmpty()) {
                prop.setProperty(property, envProperty);
            }
        }
        return prop;
    }

    public static String get(String key) {
        return getProp().getProperty(key);
    }
}
