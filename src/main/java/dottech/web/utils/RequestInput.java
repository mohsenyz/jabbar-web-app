package dottech.web.utils;

import spark.Request;
import spark.Response;

import javax.servlet.http.Part;

public class RequestInput {

    public static int asLongOrFail(Request request, Response response, String key) {
        String value = request.params(key);

        if (value == null || value.trim().isEmpty() || !Validator.isInt(value)) {
            response.status(500);
            throw new RuntimeException(
                    new StringBuilder()
                            .append("\"")
                            .append(key)
                            .append("\"")
                            .append(" doesnt found")
                            .toString()
            );
        }

        return Integer.parseInt(value);
    }


    public static String asStringInputOrFail(Request request, Response response, String key) {
        String value = request.queryParams(key);

        if (value == null || value.trim().isEmpty()) {
            response.status(500);
            throw new RuntimeException(
                    new StringBuilder()
                            .append("\"")
                            .append(key)
                            .append("\"")
                            .append(" doesnt found")
                            .toString()
            );
        }

        return value;
    }

    public static String asStringOrFail(Request request, Response response, String key) {
        String value = request.params(key);

        if (value == null || value.trim().isEmpty()) {
            response.status(500);
            throw new RuntimeException(
                    new StringBuilder()
                            .append("\"")
                            .append(key)
                            .append("\"")
                            .append(" doesnt found")
                            .toString()
            );
        }

        return value;
    }

    public static Part asPartOrFail(Request request, Response response, String key) {
        try {
            return request.raw().getPart(key);
        } catch (Exception e) {
            response.status(500);
            throw new RuntimeException(
                    new StringBuilder()
                            .append("\"")
                            .append(key)
                            .append("\"")
                            .append(" doesnt found")
                            .toString()
            );
        }
    }

}
