package dottech.web.controllers;

import dottech.web.dao.PostDao;
import dottech.web.dao.UserDao;
import dottech.web.pojos.Post;
import dottech.web.pojos.User;
import dottech.web.utils.*;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class PostController {

    public static String newPost(Request request, Response response) {
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement("C:\\Users\\" + System.getProperty("user.name") + "\\AppData\\Local\\Temp");
        request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);

        String description = request.queryParams("description");
        Part part = RequestInput.asPartOrFail(request, response, "image");

        UserDao userDao = new UserDao(HibernateUtils.getSessionFactory());
        User user = User.authenticateOrFail(request, response, userDao);

        if (part.getSize() >= AppConfig.maxImageSize()) {
            response.status(500);
            throw new RuntimeException("Image reached max size");
        }

        String fileHash =  RandomGenerator.randomAlphaNumeric(64);
        String[] fileParts = part.getSubmittedFileName().split("\\.");
        String fileFormat = fileParts[fileParts.length - 1];

        try (final InputStream in = part.getInputStream()) {
            IOUtils.copy(in, new FileOutputStream(
                    AppProperties.get(AppProperties.KEY_UPLLOAD_DIR) + fileHash + "." + fileFormat)
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        Post post = new Post();
        post.descriptions = description;
        post.images_hashes = fileHash + "." + fileFormat;
        post.created_at = System.currentTimeMillis() / 1000l;
        post.creator_id = user.id;

        PostDao postDao = new PostDao(HibernateUtils.getSessionFactory());
        postDao.save(post);

        response.status(200);
        return "200";
    }

}
