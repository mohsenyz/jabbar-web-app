package dottech.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dottech.web.Main;
import dottech.web.dao.PostDao;
import dottech.web.pojos.Post;
import dottech.web.utils.HibernateUtils;
import dottech.web.utils.Jsonify;
import javafx.geometry.Pos;
import spark.Request;
import spark.Response;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MainController {

    public static String getMainPage(Request request, Response response) throws Exception {
        response.header("Content-Type", "application/json");
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File(Paths.get("src", "main", "resources").toFile(), "test.json")
                        )
                )
        );
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        return stringBuilder.toString();
    }


    public static String getPosts(Request request, Response response) {
        PostDao postDao = new PostDao(HibernateUtils.getSessionFactory());
        try {
            return Jsonify.returnJson(response, postDao.findAll());
        } catch (Exception e) {
            e.printStackTrace();
            response.status(500);
            return "200";
        }
    }

}
