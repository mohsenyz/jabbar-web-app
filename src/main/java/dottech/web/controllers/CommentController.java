package dottech.web.controllers;

import dottech.web.dao.CommentDao;
import dottech.web.dao.UserDao;
import dottech.web.pojos.Comment;
import dottech.web.pojos.User;
import dottech.web.utils.HibernateUtils;
import dottech.web.utils.Jsonify;
import dottech.web.utils.RequestInput;
import dottech.web.utils.Validator;
import spark.Request;
import spark.Response;

public class CommentController {

    public static String getCommentsByPost(Request request, Response response) throws Exception {

        long postId = RequestInput.asLongOrFail(request, response, "post_id");

        CommentDao commentDao = new CommentDao(HibernateUtils.getSessionFactory());

        return Jsonify.returnJson(response, commentDao.findByPost(postId));
    }


    public static String newComment(Request request, Response response) {

        long postId = RequestInput.asLongOrFail(request, response, "post_id");

        String commentStr = RequestInput.asStringInputOrFail(request, response, "comment");

        UserDao userDao = new UserDao(HibernateUtils.getSessionFactory());
        User user = User.authenticateOrFail(request, response, userDao);

        Comment comment = new Comment();
        comment.message = commentStr;
        comment.created_at = System.currentTimeMillis() / 1000l;
        comment.post_id = postId;
        comment.user_id = user.id;

        CommentDao commentDao = new CommentDao(HibernateUtils.getSessionFactory());
        commentDao.save(comment);

        response.status(200);
        return "200";
    }

}
