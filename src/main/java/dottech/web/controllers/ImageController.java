package dottech.web.controllers;

import dottech.web.utils.AppProperties;
import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

public class ImageController {


    public static Object serveImage(Request request, Response response) {
        String fileName = request.params("image");
        File sourceFile = new File(AppProperties.get(AppProperties.KEY_UPLLOAD_DIR), fileName);
        if (!sourceFile.exists()) {
            response.status(404);
            return "";
        }
        try {
            response.status(200);
            String mimeType = URLConnection.guessContentTypeFromName(sourceFile.getName());
            response.type(mimeType);
            response.header("Cache-Control", "max-age=2592000, public");
            IOUtils.copy(new FileInputStream(sourceFile), response.raw().getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
