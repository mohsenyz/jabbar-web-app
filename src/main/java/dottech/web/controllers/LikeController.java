package dottech.web.controllers;

import dottech.web.dao.LikeDao;
import dottech.web.dao.UserDao;
import dottech.web.pojos.Like;
import dottech.web.pojos.User;
import dottech.web.utils.HibernateUtils;
import dottech.web.utils.RequestInput;
import spark.Request;
import spark.Response;

public class LikeController {

    public static String likePost(Request request, Response response) {

        long postId = RequestInput.asLongOrFail(request, response, "post_id");

        UserDao userDao = new UserDao(HibernateUtils.getSessionFactory());
        User user = User.authenticateOrFail(request, response, userDao);

        LikeDao likeDao = new LikeDao(HibernateUtils.getSessionFactory());
        boolean isLikedNow = likeDao.toggleLikeByPost(postId, user);

        if (isLikedNow) {
            response.status(200);
        } else {
            response.status(201);
        }

        return "200";
    }


    public static String likeStatusByPost(Request request, Response response) {

        long postId = RequestInput.asLongOrFail(request, response, "post_id");

        UserDao userDao = new UserDao(HibernateUtils.getSessionFactory());
        User user = User.authenticateOrFail(request, response, userDao);

        LikeDao likeDao = new LikeDao(HibernateUtils.getSessionFactory());
        Like like = likeDao.findByPost(postId, user.id);

        if (like == null)
            response.status(201);
        else
            response.status(200);
        return "200";
    }

}
