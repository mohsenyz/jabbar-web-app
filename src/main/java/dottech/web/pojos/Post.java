package dottech.web.pojos;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public Long creator_id;
    public String descriptions;
    public String images_hashes;
    public Long created_at;
    public Long views = 0L;

    public void setImages(String[] imagesHashes) {
        setImages(Arrays.asList(imagesHashes));
    }

    public void setImages(List<String> imagesHashes) {
        StringJoiner stringJoiner = new StringJoiner(",");
        for (String imageHash : imagesHashes) {
            stringJoiner.add(imageHash);
        }
        this.images_hashes = stringJoiner.toString();
    }

}
