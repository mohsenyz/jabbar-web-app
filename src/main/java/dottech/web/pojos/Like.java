package dottech.web.pojos;

import javax.persistence.*;

@Entity
@Table(name = "likes")
public class Like {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public Long post_id;
    public Long user_id;
    public Long created_at;

}
