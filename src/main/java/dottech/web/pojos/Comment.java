package dottech.web.pojos;


import org.omg.CORBA.portable.IDLEntity;

import javax.persistence.*;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public Long user_id;
    public String message;
    public Long created_at;
    public Long post_id;
}
