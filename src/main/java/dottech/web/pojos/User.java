package dottech.web.pojos;

import dottech.web.dao.UserDao;
import dottech.web.utils.RequestInput;
import org.hibernate.SessionFactory;
import spark.Request;
import spark.Response;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;
    public String phone;
    public String verification_code;
    public Long code_sent_time;
    public boolean is_verified;
    public Long created_at;
    public String token;


    public static User authenticateOrFail(Request request, Response response, UserDao userDao) {
        User user = userDao.findByToken(RequestInput.asStringOrFail(request, response, "token"));
        if (user == null) {
            response.status(500);
            throw new RuntimeException("User token isn't valid");
        }

        return user;
    }

}
